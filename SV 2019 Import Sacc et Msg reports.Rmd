---
title: "SV 2019 Import Sacc et Msg Reports"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(eval=TRUE,echo = FALSE, include = F)
```
# Pr�paration des donn�es


----------

### Import

####import packages

```{r}
library(tidyverse)
library(markdown)
library(rmarkdown)
library(magrittr)
library(lubridate)
library(readxl)
library(readr)
library(lubridate)

```


####import des fichiers 

sacc_report

```{r}

saccReport_svb104a1OD <- read_delim("Reports_SV_BW_2019/Output/svb104a1OD.xls","\t", escape_double = FALSE, trim_ws = TRUE)
sacc_report<-saccReport_svb104a1OD
```

msg_report

```{r}
msgReport_svb104a1OD <- read_delim("Reports_SV_BW_2019/Output/svb104a1ODMsg.xls","\t", escape_double = FALSE, trim_ws = TRUE)
msg_report<-msgReport_svb104a1OD
```


fichier dates naissance - on s'assure colonnes dates est au format date
```{r}

dates <- read_excel("dates.xlsx", col_types = c("text", 
    "date", "date", "text", "text", "text", 
    "text", "date", "text", "numeric", "text"))
dates$`Date Naiss`<-(as.character(dates$`Date Naiss`))
dates$`Date Pass 1`<-(as.character(dates$`Date Pass 1`))
dates$`Date Pass 2`<-(as.character(dates$`Date Pass 2`))


```


### Filtrage des donn�es

#### suppression essais "recycle" et "."
dans sacc_report
<br/>dans msg_report


```{r}
sacc_report<- filter(sacc_report,VAR_essaiCorrect=="ok" | VAR_essaiCorrect=="suivant")
msg_report<- filter(msg_report,VAR_essaiCorrect=="ok" | VAR_essaiCorrect=="suivant")

```

#### oeil choisi 

On s'assure oeil d�but = oeil fin et on le d�finit dans la variable : oeil


```{r, include=TRUE, eval=TRUE}
oeil<-sacc_report$EYE_USED[nrow(sacc_report)]
oeildeb<-sacc_report$EYE_USED[1]
mess_oeil<- if_else(oeil!= oeildeb, paste("ATTENTION l'oeil choisi n'est pas le m�me au d�but et � la fin"),paste("L'oeil choisi est le " ,oeil))
mess_oeil

### ----------------   voir ici s'il ne faut pas g�rer variable oeil quand diff�rent ----------

```

on filtre sur l'oeil choisi

```{r,}
sacc_report<- filter(sacc_report,EYE_USED==oeil)
msg_report<- filter(msg_report,EYE_USED==oeil)
```
#### choix des colonnes

```{r}
sacc_report<- select(sacc_report,RECORDING_SESSION_LABEL,EYE_USED,TRIAL_INDEX, TRIAL_SACCADE_TOTAL, preada_condition, preada_x_drift, preada_y_drift,preada_x_ciblepre,  preada_y_ciblepre, Vid_Loc, T1_Loc,  T2_Loc,VAR_cptAR, VAR_choix, VAR_essaiCorrect, VAR_n, CURRENT_SAC_INDEX,  CURRENT_SAC_CONTAINS_BLINK,CURRENT_SAC_DIRECTION, CURRENT_SAC_ANGLE, CURRENT_SAC_AMPLITUDE, CURRENT_SAC_DURATION, CURRENT_SAC_AVG_VELOCITY, CURRENT_SAC_PEAK_VELOCITY, CURRENT_SAC_START_TIME, CURRENT_SAC_START_X, CURRENT_SAC_START_Y, CURRENT_SAC_END_X, CURRENT_SAC_END_Y )
```


### Calculs nouvelles variables pour tableau final

```{r}
#nomFichier
mon_nom_fichier<-as.character(sacc_report[2,1])
#participant
mon_nom_participant<-substr(sacc_report[2,1],4,6)
#calcul num�ro de passation pour ordre et choix colonne recherche dans dates
mon_ordre<-as.numeric(substr(sacc_report[2,1],8,8))
#ma_condition de passation (ada ou Nostep)
ma_condition<-if_else(substr(sacc_report[2,1],7,7)=="a","ada","noStep")
#recherche date_naiss
ma_date_naiss <-as.character(dates%>%filter(`Code Sjt 1`==mon_nom_fichier) %>% select(`Date Naiss`))
#recherche date passation
ma_date_pass <-as.character(ifelse(mon_ordre==1,(dates%>%filter(`Code Sjt 1`==mon_nom_fichier) %>% select(`Date Pass 1`)),(dates%>%filter(`Code Sjt 2`==mon_nom_fichier) %>% select(`Date Pass 2`))))
#recherche sexe
mon_sexe<-as.character(dates%>%filter(`Code Sjt 1`==mon_nom_fichier) %>% select(`Sexe`))
#groupe (adulte/bb)
mon_age<-time_length(interval(ma_date_naiss,ma_date_pass),unit = "years")
mon_groupe<- if_else(mon_age>16,"adulte","bebe")

```


### Renommage et cr�ation nouvelles colonnes

changement nom variable RECORDING_SESSION_LABEL --> fichiers

participants: substring(num�ro dans cellule nom de fichier)
date_naiss,date_pass, sexe, groupe, condition de passation, code essais �limin�s

```{r}
#renommage colonne RECORDING_SESSION_LABEL en fichiers
sacc_report<- rename(sacc_report,fichiers=RECORDING_SESSION_LABEL)

##ajout
#participant (num�ro � partir nom fichier)
 sacc_report<- add_column(sacc_report, participants=mon_nom_participant, .before = 1)
#date_naiss
 sacc_report<-add_column(sacc_report, dates_naiss=ma_date_naiss, .after = 2)
#date_passation
 sacc_report<-add_column(sacc_report, dates_pass=ma_date_pass, .after = 3)
#sexe
 sacc_report<-add_column(sacc_report, sexe=mon_sexe, .after = 4)
#groupe
 sacc_report<-add_column(sacc_report, groupe=mon_groupe, .after = 5)
#condition passation
 sacc_report<-add_column(sacc_report, cond_pass=ma_condition, .after = 6)
#ordre passation
 sacc_report<-add_column(sacc_report, ordre_pass=mon_ordre, .after = 7)
#code essais  
 sacc_report<-add_column(sacc_report, code_essais="", .after = 9)
#Temps d�part saccade 
 sacc_report<-add_column(sacc_report, TpsDepSacc="", .after = ncol(sacc_report))
#TpsAffichageCible (top d�part saccade)
 sacc_report<-add_column(sacc_report, TpsAffichCible="", .after = ncol(sacc_report))
# latence
 sacc_report<-add_column(sacc_report, latences="", .after = ncol(sacc_report))
#amplitude
 sacc_report<-add_column(sacc_report, amplitudes="", .after = ncol(sacc_report))
 
 
```


### cr�ation fichiers sortie

indiv (fichier avec toutes les donn�es calcul�es)


```{r}
indiv <-data.frame (matrix(ncol = length(colnames(sacc_report)), nrow=180))
colnames(indiv)<-colnames(sacc_report)

# puis mettre la bonne saccade pour chaque ligne et � la fon enregistrer indiv+num participant##########

```


#Saccade

On cherche la bonne saccade et on enregistre pourquoi on les exclues dans code essais
on coupe sacc_report et Msg_Reporten autant de tableaux que de saccindex

```{r}
dfSacc<-split(sacc_report, as.factor(sacc_report$TRIAL_INDEX))
dfMsg<-split(msg_report, as.factor(msg_report$TRIAL_INDEX))
```

30/04/19
il faudra ensuite aller voir dans chaque trial quelle saccade on garde et sinon, pourquoi on l'�limine

```{r}
dfSaccpurr<- map(dfSacc,as.tibble)
dfMsgpurr<- map(dfMsg,as.tibble)
mon_index<-  length(dfSacc)



for (index in 1:mon_index) {

  
}

index=22 # sera remplac� par boucle au dessus : de index==1 � index== nbre saccade la suite � mettre dans boucle pour faire dans chaque saccade ou faire un apply de tout ce qui suit

mon_nbSacc <- dfSaccpurr[[index]][[12]][1]  #on regarde pour saccade TRIAL_SACC_TOTAL et on met dans variable mon_nbSacc
mini_dfSacc<-dfSaccpurr[[index]]
mini_dfMsg<- dfMsgpurr[[index]]

mini_dfMsg_tmp<-mini_dfMsg %>% filter(grepl("[0-9]", CURRENT_MSG_SAC_START_TIME)) #on filtre pour avoir temps d�part sacc

Mon_TpsDepSacc<- as.integer(mini_dfMsg_tmp[[26]][1])

#on cherche bonne saccade : on filtre : il faut une amplitude sup�rieur � 3�
mini_dfSacc_tmp<-mini_dfSacc %>% filter(CURRENT_SAC_AMPLITUDE>3)

nbSaccMini<-length(mini_dfSacc_tmp[[1]])
var_msg<-ifelse(nbSaccMini<1,"pas de saccades de plus de 3�","")
var_msg


mini_dfMsg_tmp<-mini_dfMsg %>% filter(grepl("^D", CURRENT_MSG_TEXT))%>%filter(!grepl("Seul", CURRENT_MSG_TEXT)) # on filtre pour avoir ligne displayfixation et donc signal d�pat pour saccade (affichage cible)

mon_TpsAffichCible<-mini_dfMsg_tmp[[23]]



ma_latence<- Mon_TpsDepSacc- mon_TpsAffichCible     #on calcul latence � partir de ces temps




```

